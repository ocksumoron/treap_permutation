//
//  stupid.h
//  treap_permutation
//
//  Created by Даня on 10.03.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef treap_permutation_stupid_h
#define treap_permutation_stupid_h

#include <vector>
#include "treap_interface.h"

using std::vector;

class StupidStruct: public IPermutableStruct {
    
    vector <int> data;
    

    
public:
    
    ~StupidStruct() {
    }
    
    void insert(int value, size_t index) {
        data.insert(data.begin() + index, value);
    }
    
    void assign(int value, size_t index) {
        data[index] = value;
    }
    
    ll subsegmentSum(size_t left, size_t right) {
        ll result = 0;
        return std::accumulate(data.begin() + left, data.begin() + right + 1, result);
    }
    
    bool nextPermutation(size_t left, size_t right) {
        return next_permutation(data.begin() + left, data.begin() + right + 1);
    }
    
    vector <int> getData() {
        return data;
    }
    
    void print() {
        for (int i = 0; i < data.size(); ++i) {
            std::cout << data[i] << " ";
        }
    }
    
    ui getSize() {
        return data.size();
    }
    
    
};

#endif
