//
//  random_functions.h
//  treap_permutation
//
//  Created by Даня on 04.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef treap_permutation_random_functions_h
#define treap_permutation_random_functions_h

std::default_random_engine generator;

typedef long long ll;
typedef unsigned int ui;

ui generateRandom(ui minx, ui maxx) {
    std::uniform_int_distribution<ui> distribution(minx, maxx);
    return distribution(generator);
}

int generateRandomValue() {
    std::uniform_int_distribution<int> distribution(INT_MIN, INT_MAX);
    return distribution(generator);
}

#endif
