//
//  treap_interface.h
//  treap_permutation
//
//  Created by Даня on 10.03.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef treap_permutation_treap_interface_h
#define treap_permutation_treap_interface_h

#include <random>
#include <vector>
#include "random_functions.h"



using std::vector;

class IPermutableStruct {

public:
    virtual void insert(int value, size_t index) = 0;
    virtual void assign(int value, size_t index) = 0;
    virtual ll subsegmentSum(size_t left, size_t right) = 0;
    virtual bool nextPermutation(size_t left, size_t right) = 0;
    virtual vector <int> getData() = 0;
    virtual ui getSize() = 0;
    virtual ~IPermutableStruct() {}
};



#endif
