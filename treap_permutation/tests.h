//
//  tests.h
//  treap_permutation
//
//  Created by Даня on 12.03.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef treap_permutation_tests_h
#define treap_permutation_tests_h

#include "treap_interface.h"
#include "treap.h"
#include "stupid.h"
#include "random_functions.h"

enum ETestType {
    INSERT,
    ASSIGN,
    SUBSEGMENT_SUM,
    NEXT_PERMUTATION,
    TOTAL_NUMBER
};

class Test {
    
    struct TestStruct {
        ETestType testType;
        ui left, right;
        int value;
        TestStruct(ETestType testType, ui left, ui right, int value): testType(testType), left(left), right(right), value(value) {}
    };
    
    struct Indexes {
        ui left, right;
        Indexes() {}
        Indexes(ui left, ui right): left(fmin(left, right)), right(fmax(left, right)){}
    };
    
    Indexes generateIndex(ui size, ui left, ui right) {
        ui leftIndex, rightIndex;
        leftIndex = fmin(size - 1, generateRandom(left, right));
        rightIndex = fmin(size - 1, generateRandom(left, right));
        return Indexes(leftIndex, rightIndex);
    }
    
    vector <TestStruct> generateTests(ui amount, ui left, ui right) {
        vector <TestStruct> result;
        result.push_back(TestStruct(INSERT, 0, 0, generateRandomValue()));
        ui size = 1;
        ui index;
        Indexes leftright;
        for (int i = 1; i < amount; ++i) {
            ui diceRoll = generateRandom(0, TOTAL_NUMBER - 1);
            switch (diceRoll) {
                case INSERT:
                    index = generateRandom(0, size);
                    result.push_back(TestStruct(INSERT, index, index, generateRandomValue()));
                    size++;
                    break;
                case ASSIGN:
                    index = generateRandom(0, size - 1);
                    result.push_back(TestStruct(ASSIGN, index, index, generateRandomValue()));
                    break;
                case SUBSEGMENT_SUM:
                    leftright = generateIndex(size, left, right);
                    result.push_back(TestStruct(SUBSEGMENT_SUM, leftright.left, leftright.right, 0));
                    break;
                case NEXT_PERMUTATION:
                    leftright = generateIndex(size, left, right);
                    result.push_back(TestStruct(NEXT_PERMUTATION, leftright.left, leftright.right, 0));
                    break;
                default:
                    break;
            }
        }
        return result;
    }
    
    vector <TestStruct> generatePermutationTests(ui amount, ui left, ui right) {
        vector <TestStruct> result;
        result.push_back(TestStruct(INSERT, 0, 0, generateRandomValue()));
        ui size = 1;
        ui index;
        Indexes leftright;
        for (int i = 1; i < amount; ++i) {
            ui diceRoll = generateRandom(0, 1);
            if (diceRoll) {
                leftright = generateIndex(size, left, right);
                result.push_back(TestStruct(NEXT_PERMUTATION, leftright.left, leftright.right, 0));
            } else {
                index = generateRandom(0, size);
                result.push_back(TestStruct(INSERT, index, index, generateRandomValue()));
                size++;
            }
        }
        return result;
    }

    vector <ll> proceedTest(IPermutableStruct &realization, vector <TestStruct> &tests) {
        vector <ll> result;
        for (int i = 0; i < tests.size(); ++i) {
            switch (tests[i].testType) {
                case INSERT:
                    realization.insert(tests[i].value, tests[i].left);
                    break;
                case ASSIGN:
                    realization.assign(tests[i].value, tests[i].left);
                    break;
                case SUBSEGMENT_SUM:
                    result.push_back(realization.subsegmentSum(tests[i].left, tests[i].right));
                    break;
                case NEXT_PERMUTATION:
                    result.push_back(realization.nextPermutation(tests[i].left, tests[i].right));
                    break;
            }
        }
        return result;
    }
    
    bool checkTests(vector <TestStruct> &tests, IPermutableStruct &realizationA, IPermutableStruct &realizationB) {
        vector <ll> resultA = proceedTest(realizationA, tests);
        vector <ll> resultB = proceedTest(realizationB, tests);
        return resultA == resultB;
    }
    
    
public:
    
    Test() {
    }
    
    ~Test() {
    }

    bool checkResults(IPermutableStruct &realizationA, IPermutableStruct &realizationB) {
        vector <TestStruct> testsAll = generateTests(100000, -1000000, 100000);
        vector <TestStruct> testsPermutation = generatePermutationTests(100000, -1543, 2015);
        return checkTests(testsAll, realizationA, realizationB) && checkTests(testsPermutation, realizationA, realizationB);
    }
};

bool TreapVSStupid() {
    Treap treap = Treap();
    StupidStruct stupid = StupidStruct();
    Test test = Test();
    return test.checkResults(treap, stupid);
}

#endif
