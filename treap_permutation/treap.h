//
//  treap.h
//  treap_permutation
//
//  Created by Даня on 10.03.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef treap_permutation_treap_h
#define treap_permutation_treap_h

#include <vector>
#include "treap_interface.h"

using std::make_pair;
using std::pair;
using std::swap;
using std::cout;
using std::vector;

const ui INF = 1000000000;

class Treap: public IPermutableStruct {
    
    struct Node {
        int priority, value, left_value, right_value;
        ll sum;
        ui size, decreasing_end_length, increasing_begin_length;
        bool needReverse;
        Node *left, *right;
        
        Node(int priority, int value): priority(priority), value(value), sum(value), size(1), left_value(value), right_value(value), decreasing_end_length(1), needReverse(false), left(NULL), right(NULL) {}
        explicit Node(int value): Node(generateRandomValue(), value) {}
        ~Node() {
            delete left;
            delete right;
        }
        
    };
    
//    struct NodeEndInfo {
//        Node *node;
//        ui sizeOfEnd;
//        int valueOfEnd;
//        bool isFullMonotone;
//        NodeEndInfo(Node *node, bool isSuffixNeeded, bool isFirst) {
//            if (node) {
//                if (isSuffixNeeded) {
//                    sizeOfEnd = node->decreasing_end_length;
//                    if (isFirst)
//                        valueOfEnd = node->left_value;
//                    else
//                        valueOfEnd = node->right_value;
//                } else {
//                    sizeOfEnd = node->increasing_begin_length;
//                    if (isFirst)
//                        valueOfEnd = node->right_value;
//                    else
//                        valueOfEnd = node->left_value;
//                }
//                isFullMonotone = sizeOfEnd == node->size;
//            } else {
//                sizeOfEnd = 0;
//                if (isFirst)
//                    valueOfEnd = INT_MAX;
//                else
//                    valueOfEnd = INT_MIN;
//                isFullMonotone = true;
//            }
//        }
//    };
    
    struct ThreeNode {
        Node *firstNode, *secondNode, *thirdNode;
//        ThreeNode(): firstNode(NULL), secondNode(NULL), thirdNode(NULL) {}
        ~ThreeNode() {
        }
    };
    
    struct TwoNode {
        Node *firstNode, *secondNode;
//        TwoNode(): firstNode(NULL), secondNode(NULL) {}
        ~TwoNode() {
        }
    };
    
    int getLeftValue(Node *node) {
        if (!node)
            return 0;
        return node->needReverse ? node->right_value : node->left_value;
    }
    
    int getRightValue(Node *node) {
        if (!node)
            return 0;
        return node->needReverse ? node->left_value : node->right_value;
    }
    
    ui getDecreasingEndLength(Node *node) {
        if (!node)
            return 0;
        return node->needReverse ? node->increasing_begin_length : node->decreasing_end_length;
    }
    
    ui getIncreasingBeginLength(Node *node) {
        if (!node)
            return 0;
        return node->needReverse ? node->decreasing_end_length : node->increasing_begin_length;
    }
    
    ui nodeSize(Node *node) {
        return node ? node->size : 0;
    }
    
    ll nodeSum(Node *node) {
        return node ? node->sum : 0;
    }
    
    void changeReverse(Node *node) {
        if (node)
            node->needReverse ^= 1;
    }
    
    void relax(Node *node) {
        if (!node)
            return;
        if (node->needReverse) {
            swap(node->left, node->right);
            swap(node->decreasing_end_length, node->increasing_begin_length);
            swap(node->left_value, node->right_value);
            changeReverse(node->left);
            changeReverse(node->right);
            node->needReverse = false;
        }
    }
    
    void updateLeftAndRightValue(Node *node) {
        node->left_value = node->left ? getLeftValue(node->left) : node->value;
        node->right_value = node->right ? getRightValue(node->right) : node->value;
    }
    
    void updateSize(Node *node) {
        node->size = 1 + nodeSize(node->left) + nodeSize(node->right);
    }
    
    void updateSum(Node *node) {
        node->sum = node->value + nodeSum(node->left) + nodeSum(node->right);
    }
    
    void updateSizeMax(Node *node, ui &sizeToUpdate, Node *first, ui firstEndLenght, int firstEndValue, Node *second, ui secondEndLength, int secondEndValue)
    {
        sizeToUpdate = 1;
        if (first)
        {
            sizeToUpdate = firstEndLenght;
            if (first->size != sizeToUpdate) {
                return;
            }
            if (firstEndValue <= node->value) {
                sizeToUpdate++;
            } else {
                return;
            }
        }
        if (second && secondEndValue >= node->value)
            sizeToUpdate += secondEndLength;
    }
    
    void updatePrefixAndSuffixLength(Node *node)
    {
        
        updateSizeMax(node, node->increasing_begin_length, node->left, getIncreasingBeginLength(node->left),  getRightValue(node->left), node->right, getIncreasingBeginLength(node->right), getLeftValue(node->right));
        updateSizeMax(node, node->decreasing_end_length, node->right, getDecreasingEndLength(node->right), getLeftValue(node->right), node->left, getDecreasingEndLength(node->left), getRightValue(node->left));
    }

//    void updatePrefixAndSuffixLength(Node *node) {
//        if (!node)
//            return;
//        if (!node->left && !node->right) {
//            node->decreasing_end_length = node->increasing_begin_length = 1;
//        } else if (!node->left) {
//            node->increasing_begin_length = 1;
//            node->decreasing_end_length = getDecreasingEndLength(node->right);
//            if (getLeftValue(node->right) >= node->value) {
//                node->increasing_begin_length += getIncreasingBeginLength(node->right);
//            }
//            if (nodeSize(node->right) == getDecreasingEndLength(node->right) && node->value >= getLeftValue(node->right)) {
//                node->decreasing_end_length++;
//            }
//        } else if (!node->right) {
//            node->increasing_begin_length = getIncreasingBeginLength(node->left);
//            node->decreasing_end_length = 1;
//            if (getRightValue(node->left) >= node->value) {
//                node->decreasing_end_length += getDecreasingEndLength(node->left);
//            }
//            if (nodeSize(node->left) == getIncreasingBeginLength(node->left) && node->value >= getRightValue(node->left)) {
//                node->increasing_begin_length++;
//            }
//        } else {
//            if (getDecreasingEndLength(node->right) < nodeSize(node->right)) {
//                node->decreasing_end_length = getDecreasingEndLength(node->right);
//            } else {
//                if (node->value >= getLeftValue(node->right)) {
//                    node->decreasing_end_length = nodeSize(node->right) + 1;
//                    if (node->value <= getRightValue(node->left)) {
//                        node->decreasing_end_length += getDecreasingEndLength(node->left);
//                    }
//                } else {
//                    node->decreasing_end_length = nodeSize(node->right);
//                }
//            }
//            if (getIncreasingBeginLength(node->left) < nodeSize(node->left)) {
//                node->increasing_begin_length = getIncreasingBeginLength(node->left);
//            } else {
//                if (node->value >= getRightValue(node->left)) {
//                    node->increasing_begin_length = nodeSize(node->left) + 1;
//                    if (node->value <= getLeftValue(node->right)) {
//                        node->increasing_begin_length += getIncreasingBeginLength(node->right);
//                    }
//                } else {
//                    node->increasing_begin_length = nodeSize(node->left);
//                }
//            }
//            
//        }
//    }
    
    void update(Node *node) {
        if (!node)
            return;
        relax(node);
        updateLeftAndRightValue(node);
        updateSize(node);
        updateSum(node);
        updatePrefixAndSuffixLength(node);
//        node->size = 1;
//        node->sum = node->right_value = node->left_value = node->value;
//        node->decreasing_end_length = 1;
//        if (node->left) {
//            node->size += node->left->size;
//            node->sum += node->left->sum;
//            node->left_value = node->left->left_value;
//        }
//        if (node->right) {
//            node->size += node->right->size;
//            node->sum += node->right->sum;
//            node->right_value = node->right->right_value;
//            node->decreasing_end_length = node->right->decreasing_end_length;
//            if (node->right->decreasing_end_length == node->right->size) {
//                if (node->value >= node->right->left_value) {
//                    node->decreasing_end_length = node->right->size + 1;
//                }
//            }
//        }
//        if (node->left && node->value <= node->left->right_value && (!node->right || (node->decreasing_end_length == node->right->size + 1))) {
//            node->decreasing_end_length += node->left->decreasing_end_length;
//        }
    }
    

    
    Node* merge(Node *leftTreap, Node *rightTreap) {
        //relax(leftTreap);
        //relax(rightTreap);
        update(leftTreap);
        update(rightTreap);
        if (!leftTreap || !rightTreap)
            return leftTreap ? leftTreap : rightTreap;
        if (leftTreap->priority > rightTreap->priority) {
            leftTreap->right = merge(leftTreap->right, rightTreap);
            update(leftTreap);
            return leftTreap;
        } else {
            rightTreap->left = merge(leftTreap, rightTreap->left);
            update(rightTreap);
            return rightTreap;
        }
    }
    
    TwoNode split(Node *root, ui sizeLeft) {
        update(root);
        //relax(root);
        if (!sizeLeft)
            return TwoNode{NULL, root};
        if (root->size == sizeLeft)
            return TwoNode{root, NULL};
        if (nodeSize(root->left) >= sizeLeft) {
            TwoNode splitResult = split(root->left, sizeLeft);
            root->left = splitResult.secondNode;
            update(root);
            return TwoNode{splitResult.firstNode, root};
        } else {
            TwoNode splitResult = split(root->right, sizeLeft - nodeSize(root->left) - 1);
            root->right = splitResult.firstNode;
            update(root);
            return TwoNode{root, splitResult.secondNode};
        }
    }
    
    ThreeNode splitByValueInSuffix(Node *root, int value) {
        //relax(root);
        //update(root);
        if (!root)
            return ThreeNode();
        if (root->size == 1) {
            if (root->value > value) {
                return ThreeNode{NULL, root, NULL};
            } else {
                return ThreeNode{NULL, NULL, root};
            }
        }
        if (root->value > value) {
            ThreeNode rightPart = splitByValueInSuffix(root->right, value);
            if (rightPart.secondNode) {
                root->right = rightPart.firstNode;
                update(root);
                return ThreeNode{root, rightPart.secondNode, rightPart.thirdNode};
            } else {
                return twoSplits(root, nodeSize(root->left), 1);
            }
        } else {
            ThreeNode leftPart = splitByValueInSuffix(root->left, value);
            root->left = leftPart.thirdNode;
            update(root);
            return ThreeNode{leftPart.firstNode, leftPart.secondNode, root};
        }
    }
    
    ThreeNode twoSplits(Node *root, ui sizeLeft, ui sizeMiddle) {
//        ThreeNode result;
        //update(root);
        TwoNode firstSplitResult = split(root, sizeLeft);
        TwoNode secondSplitResult = split(firstSplitResult.secondNode, sizeMiddle);
//        result.firstNode = firstSplitResult.firstNode;
//        result.secondNode = secondSplitResult.firstNode;
//        result.thirdNode = secondSplitResult.secondNode;
        return ThreeNode{firstSplitResult.firstNode, secondSplitResult.firstNode, secondSplitResult.secondNode};
    }
    
    Node* twoMerges(ThreeNode &nodes) {
        //update(nodes.firstNode);
        //update(nodes.secondNode);
        //update(nodes.thirdNode);
        nodes.firstNode = merge(nodes.firstNode, nodes.secondNode);
        nodes.firstNode = merge(nodes.firstNode, nodes.thirdNode);
        //update(nodes.firstNode);
        return nodes.firstNode;
    }
    
    int getValue(Node *root, ui index) {
        ThreeNode separareParts = twoSplits(root, index, 1);
        int result = separareParts.secondNode->value;
        root = twoMerges(separareParts);
        return result;
    }
    
    void print(Node *root) {
        update(root);
        if (root) {
            print(root->left);
            cout << root->value << " ";
            print(root->right);
        }
    }
    
    bool nextPermutation(Node *root) {
        //update(root);
        if (!root)
            return true;
        if (root->decreasing_end_length == root->size) {
            root->needReverse ^= 1;
            update(root);
            return false;
        }
        ThreeNode separateParts = twoSplits(root, root->size - root->decreasing_end_length - 1, 1);
        ThreeNode splittedSuffix = splitByValueInSuffix(separateParts.thirdNode, separateParts.secondNode->value);
        swap(separateParts.secondNode, splittedSuffix.secondNode);
        separateParts.thirdNode = twoMerges(splittedSuffix);
        separateParts.thirdNode->needReverse ^= 1;
        root = twoMerges(separateParts);
        //update(root);
        return true;
    }
    
    vector <int> getData(Node *root) {
        update(root);
        if (root) {
            getData(root->left);
            data.push_back(root->value);
            getData(root->right);
        }
        return data;
    }
    
    Node *root;
    vector <int> data;
    

public:
    
    ~Treap() {
        delete root;
    }
    
    void reverse(size_t left, size_t right) {
        ThreeNode separateParts = twoSplits(root, left, right - left);
        separateParts.secondNode->needReverse ^= 1;
        root = twoMerges(separateParts);
        //update(root);
    }
    
    void insert(int value, size_t index) {
        Node *newElement = new Node(value);
        TwoNode previousTreap = split(root, index);
        ThreeNode newTreap = ThreeNode{previousTreap.firstNode, newElement, previousTreap.secondNode};
        root = twoMerges(newTreap);
        //update(root);
//        root = merge(previousTreap.firstNode, newElement);
//        root = merge(root, previousTreap.secondNode);
    }
    
    void assign(int value, size_t index) {
        Node *newElement = new Node(value);
        ThreeNode previousTreap = twoSplits(root, index, 1);
        ThreeNode newTreap = ThreeNode{previousTreap.firstNode, newElement, previousTreap.thirdNode};
        delete previousTreap.secondNode;
//        previousTreap.second = split(previousTreap.secondNode, 1).secondNode;
//        root = merge(previousTreap.first, newElement);
//        root = merge(root, previousTreap.secondNode);
        root = twoMerges(newTreap);
        //update(root);
    }
    
    ll subsegmentSum(size_t left, size_t right) {
        ThreeNode separateParts = twoSplits(root, left, right - left + 1);
        ll result = separateParts.secondNode->sum;
        root = twoMerges(separateParts);
        //update(root);
        return result;
    }
    
    bool nextPermutation(size_t left, size_t right) {
        ThreeNode separateParts = twoSplits(root, left, right - left + 1);
        bool result = nextPermutation(separateParts.secondNode);
        root = twoMerges(separateParts);
        //update(root);
        return result;
    }
    
    void print() {
        print(root);
        cout << "\n";
    }
    
    int getValue(size_t index) {
        return getValue(root, index);
    }
    
    vector <int> getData() {
        data.clear();
        return getData(root);
    }
    
    ui getSize() {
        return root->size;
    }
    
};

#endif
